package ru.example.lab1;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleUnitTest {
    @Test
    public void secondMoreFirst() {
        assertEquals(6, Comparison.max(4, 6));
    }

    @Test
    public void firstMoreFirst(){
        assertEquals(3, Comparison.max(3, 1));
    }

    @Test
    public void identicalNumbers(){
        assertEquals(5, Comparison.max(5, 5));
    }

    @Test
    public void maxWithNegative(){
        assertEquals(-5, Comparison.max(-7, -5));
    }

    @Test
    public void secondLessFirst(){
        assertEquals(4,  Comparison.min(4, 6));
    }

    @Test
    public void firstLessFirst(){
        assertEquals(1, Comparison.min(3, 1));
    }

    @Test
    public void identicalNumbersForMin(){
        assertEquals(5, Comparison.min(5, 5));
    }

    @Test
    public void minWithNegative(){
        assertEquals(-7, Comparison.min(-7, -5));
    }
}