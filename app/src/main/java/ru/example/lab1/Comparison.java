package ru.example.lab1;


/**
 * Класс, содержащий функции min и max
 *
 * @author Карина Елагина, Алексей Кожемякин
 * @version 1.0
 */
public class Comparison {

    /**
     * Функция вычисления минимума
     *
     * @param a - первое число для анализа
     * @param b - второе число для анализа
     * @return возвращает значение минимума
     */
    public static long min(long a, long b){
        return (a > b? b: a);
    }

    /**
     * Функция вычисления максимума
     *
     * @param a - первое число для анализа
     * @param b - второе число для анализа
     * @return возвращает значение максимума
     */
    public static long max(long a, long b){
        return (a > b? a: b);
    }
}
